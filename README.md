# More-on-Data-Exploration
Welcome back to my data analysis portfolio project! In this project, I continued my exploration and analysis of the SQL database that I created earlier. 
I dug deeper into the data using advanced statistical techniques to gain more insights and make predictions. 
The project report provides a comprehensive overview of my approach, results, and recommendations. I hope you find this project informative and insightful!
